// document-querySelector -> use it to retrieve an element from the webpage
/*
	document.getelementById('txt-first-name');
	document.getelementsByClassName('txt-input');
	document.getelementsByTagName('input');
*/
const textFirstName = document.querySelector('#txt-first-name');
const textLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


// action considered as an event
textFirstName.addEventListener('keyup', (event ) => {

	spanFullName.innerHTML = textFirstName.value;

});

textFirstName.addEventListener('keyup', (event) => {

	console.log(event.target);
	console.log(event.target.value);
});

textLastName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = textFirstName.value + " " + textLastName.value;

	console.log(event.target);
	console.log(event.target.value);

});